package br.gov.etec.exemplojpa.business;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.gov.etec.exemplojpa.dao.AlunoDAO;
import br.gov.etec.exemplojpa.infra.JPAUtil;
import br.gov.etec.exemplojpa.model.Aluno;

public class AlunoRN {
	Aluno aluno = new Aluno();
	List<Aluno> alunos = new ArrayList<Aluno>();
	
	public void gravar(Aluno entity) {
		EntityManager em = new JPAUtil().getEntityManager();
        em.getTransaction().begin();
        AlunoDAO dao = new AlunoDAO(em);
        dao.adiciona(entity);
        em.getTransaction().commit();
        em.close();
	}

	public void atualizar(Aluno aluno) {
		EntityManager em = new JPAUtil().getEntityManager();
        em.getTransaction().begin();
        AlunoDAO dao = new AlunoDAO(em);
		dao.altera(aluno);
		em.getTransaction().commit();
	    em.close();
	}
	
	public void excluir(Aluno aluno) {
		EntityManager em = new JPAUtil().getEntityManager();
        em.getTransaction().begin();
        AlunoDAO dao = new AlunoDAO(em);
        Aluno alunoExcluido = dao.buscaPorId(aluno.getId());
       dao.remove(alunoExcluido);
        em.getTransaction().commit();
        em.close();
        aluno = new Aluno();
     }
	
	public List<Aluno> listar() {
		EntityManager em = new JPAUtil().getEntityManager();
        em.getTransaction().begin();
        AlunoDAO dao = new AlunoDAO(em);
        alunos = dao.lista();
        em.close();
        
		return alunos;
	}

	/*
	 * getters e setters
	 * 
	 * 
	 */

	public Aluno getEntity() {
		return aluno;
	}

	public void setEntity(Aluno entity) {
		this.aluno = entity;
	}

	public List<Aluno> getEntities() {
		return alunos;
	}

	public void setEntities(List<Aluno> entities) {
		this.alunos = entities;
	}

}


****************************************************************************************

package br.gov.etec.exemplojpa.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import br.gov.etec.exemplojpa.business.AlunoRN;
import br.gov.etec.exemplojpa.model.Aluno;

@ManagedBean
public class AlunoBean implements Serializable {

	/**
	 * default serialVersionUID
	 */
	private static final long serialVersionUID = 2321858673037123090L;

	Aluno aluno = new Aluno();
	List<Aluno> alunos = new ArrayList<Aluno>();
	List<Aluno> alunosSelecionados = new ArrayList<Aluno>();
	AlunoRN service = new AlunoRN();
		
	@PostConstruct
	public void init() {
		alunos = service.listar();
		aluno = new Aluno();

	}
	
	public void gravar() {
		if(aluno.getId()== null){
			service.gravar(aluno);
		}
        else{
        	service.atualizar(aluno);
        }
		aluno = new  Aluno();
		alunos = service.listar();
	}

	public void excluir() {
		service.excluir(this.aluno);
		alunos = service.listar();
		
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public List<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}

	public List<Aluno> getAlunosSelecionados() {
		return alunosSelecionados;
	}

	public void setAlunosSelecionados(List<Aluno> alunosSelecionados) {
		this.alunosSelecionados = alunosSelecionados;
	}
}


**************************************************************************************************

package br.gov.etec.exemplojpa.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.gov.etec.exemplojpa.model.Aluno;

public class AlunoDAO {

	private final DAO<Aluno> dao;
	
	public AlunoDAO(EntityManager em) {
		dao = new DAO<Aluno>(em,Aluno.class);
	}

	public void adiciona(Aluno t) {
		dao.adiciona(t);
	}

	public Aluno buscaPorId(Integer id) {
		return dao.buscaPorId(id);
	}

	public List<Aluno> lista() {
		return dao.lista();
	}

	public void remove(Aluno t) {
		dao.remove(t);
	}

	public void altera(Aluno t) {
		dao.altera(t);
	}		
}

*********************************************************************************************

package br.gov.etec.exemplojpa.dao;

import java.util.List;

import javax.persistence.EntityManager;

public class DAO<T> {

	private final Class<T> classe;
	private final EntityManager em;

	public DAO(EntityManager em, Class<T> classe) {
		this.em = em;
		this.classe = classe;

	}
	
	public void adiciona(T t) {
		this.em.persist(t);
	}
	
	public void remove(T t) {
		this.em.remove(t);
		//EXEMPLO 2:
		//this.em.remove(this.em.merge(t));
	}
	
	public T buscaPorId(Integer id) {
		return this.em.find(classe,id);
	}
	
	@SuppressWarnings("unchecked")
	public List<T> lista(){
		return this.em.createQuery("from "+classe.getName()).getResultList();
	}
	
	public void altera(T t) {
		em.merge(t);
	}
	
}

*******************************************************************************************

package br.gov.etec.exemplojpa.infra;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {

	private static EntityManagerFactory factory;
	static {
		factory = Persistence.createEntityManagerFactory("EXEMPLO");
	}

	public static EntityManager getEntityManager() {
		return factory.createEntityManager();
	}

	public static void close() {
		factory.close();
	}
}

*******************************************************************************************

package br.gov.etec.exemplojpa.infra;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.gov.etec.exemplojpa.model.Aluno;

public class Teste {
	public static void main(String[] args) {
		Aluno aluno = new Aluno();
        aluno.setNome("X");

        EntityManagerFactory factory = Persistence.
                    createEntityManagerFactory("exemplo");
        EntityManager manager = factory.createEntityManager();

        manager.getTransaction().begin();        
        manager.persist(aluno);
        manager.getTransaction().commit();    

        System.out.println("ID da tarefa: " + aluno.getId());

        manager.close();
    }
        
}
**************************************************************************************************

package br.gov.etec.exemplojpa.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Aluno {

	@Id
	@GeneratedValue
	private Integer id;
	private String nome;
	private String telefone;
	private String email;
	private Date dtNascimento;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDtNascimento() {
		return dtNascimento;
	}

	public void setDtNascimento(Date dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dtNascimento == null) ? 0 : dtNascimento.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + id;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((telefone == null) ? 0 : telefone.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		Aluno other = (Aluno) obj;
		if (dtNascimento == null) {
			if (other.dtNascimento != null)
				return false;
		} else if (!dtNascimento.equals(other.dtNascimento))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id != other.id)
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (telefone == null) {
			if (other.telefone != null)
				return false;
		} else if (!telefone.equals(other.telefone))
			return false;
		return true;
	}

	
}
*****************************************************************************************************************************

<persistence xmlns="http://java.sun.com/xml/ns/persistence"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://java.sun.com/xml/ns/persistence 
	http://java.sun.com/xml/ns/persistence/persistence_2_0.xsd"
	version="2.0">

	<persistence-unit name="EXEMPLO">
    <provider>org.hibernate.ejb.HibernatePersistence</provider>
   

    
    <class>br.gov.etec.exemplojpa.model.Aluno</class>
     
  <properties>
            <property name="javax.persistence.jdbc.driver" value="com.microsoft.sqlserver.jdbc.SQLServerDriver"/>
            <property name="javax.persistence.jdbc.url" value="jdbc:sqlserver://localhost;DatabaseName=PCII"/>
            <property name="javax.persistence.jdbc.user" value="sa"/>
            <property name="javax.persistence.jdbc.password" value="123456"/>
            <property name="hibernate.dialect" value="org.hibernate.dialect.SQLServerDialect"/>
            <property name="hibernate.show_sql" value="true"/>
            	<property name="hibernate.hbm2ddl.auto" value="create" />
			<property name="hibernate.show_sql" value="true" />
			<property name="hibernate.format_sql" value="true" />
            
            


		</properties>
	</persistence-unit>

</persistence>

*******************************************************************************************************************************

<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html"
	xmlns:ui="http://java.sun.com/jsf/facelets">

<h:head>

	<title>
		<ui:insert name="titulo"/>
	</title>
	
	<h:outputStylesheet library="css" name="style.css" />
</h:head>

<h:body>

	<div id="cabecalho">
		<h:graphicImage library="imagens" name="logo.png" id="logoEscola" />

		<div id="usuarioLogado">
			<h:outputText rendered="#{not empty usuarioLogado.usuario.login}"
				value="Logado como:  #{usuarioLogado.usuario.login}" />
		</div>

	</div>


	<div id="conteudo">
		<ui:insert name="corpo" />
	</div>


	<div id="rodape">
		<p>&copy;Copyright 2011. Todos os direitos reservados a
			Hor&aacute;cio Augusto.</p>
	</div>

</h:body>

</html>

**************************************************************************************************************************************

<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:h="http://java.sun.com/jsf/html"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:p="http://primefaces.org/ui"
	xmlns:ui="http://java.sun.com/jsf/facelets">

<ui:composition template="/_template.xhtml">

	<ui:define name="titulo">Cadastro de Alunos</ui:define>

	<ui:define name="corpo">

		<h2>
			<h:outputText value="Novo Aluno" rendered="#{alunoBean.aluno.id==0}" />
			<h:outputText value="Editar Aluno"
				rendered="#{alunoBean.aluno.id !=0}" />
		</h2>
		<h:form id="frmCadastroAluno">
			<h:inputHidden value="#{alunoBean.aluno.id}" />

			<fieldset>
				<legend>DADOS DO ALUNO</legend>
				<p:outputLabel value="Nome:" for="nome" />
				<p:inputText id="nome" value="#{alunoBean.aluno.nome}" />

				<p:outputLabel value="Telefone:" for="fone" />
				<p:inputMask value="#{alunoBean.aluno.telefone}"
					mask="(99) 9999-9999" id="fone" />

				<p:outputLabel value="Email:" for="email" />
				<p:inputText id="email" value="#{alunoBean.aluno.email}" />

				<p:commandButton value="Gravar" action="#{alunoBean.gravar}" update=":frmAlunos:tbAunos">
				</p:commandButton>

			</fieldset>
		</h:form>

		<h2>Listagem de Alunos</h2>
		<h:form id="frmAlunos">

			<p:dataTable id="tbAunos" var="aluno" value="#{alunoBean.alunos}" rendered="#{!empty alunoBean.alunos}">
				<p:column headerText="Id">
					<h:outputText value="#{aluno.id}" />
				</p:column>

				<p:column headerText="nome">
					<h:outputText value="#{aluno.nome}" />
				</p:column>

				<p:column headerText="email">
					<h:outputText value="#{aluno.email}" />
				</p:column>
				
				<p:column headerText="Ações">
				
				<h:commandLink action="#{alunoBean.excluir}" >
				<h:graphicImage library="imagens" name="iconExcluir.png" id="excImg" />
						<f:ajax render="@form" />
						<f:setPropertyActionListener target="#{alunoBean.aluno}"
							value="#{aluno}" />
					</h:commandLink>
			<p:spacer width="10px"></p:spacer>
				<h:commandLink >
				<h:graphicImage library="imagens" name="iconEditar.png" id="editImg" />
					<f:setPropertyActionListener target="#{alunoBean.aluno}"
						value="#{aluno}"></f:setPropertyActionListener>
					<f:ajax render=":frmCadastroAluno"></f:ajax>
				</h:commandLink>
			
				</p:column>
			</p:dataTable>
		</h:form>
	</ui:define>

</ui:composition>
</html>


*********************************************************************************************************************************************************

package br.gov.etec.exemplojpa.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Veiculo {
	@Id
	private Integer id;
	
	@ManyToMany
	@JoinTable(name = "Veiculo_Acessorios",
	joinColumns = @JoinColumn(name = "idVeiculo"),
	inverseJoinColumns = @JoinColumn(name = "idAcessorio"))
	private Set<Acessorio> acessorios;
}

*********************************************************************************************************************************************************

package br.gov.etec.exemplojpa.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Usuario {
	@Id
	private Integer id;
	
	private String login;
	
	private String senha;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="idFuncionario")	
	private Funcionario funcionario;

}
*********************************************************************************************************************************************************

package br.gov.etec.exemplojpa.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Projeto {
	@Id
	private Integer id;
	
	private String descricao;
	
}

*********************************************************************************************************************************************************

package br.gov.etec.exemplojpa.model;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class PessoaId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;

	private Integer cpf;

}

*********************************************************************************************************************************************************

package br.gov.etec.exemplojpa.model;

import java.util.List;

import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Pessoa {
	@EmbeddedId
    private PessoaId id;
		
	@OneToOne
	@JoinColumns(value = {
		    @JoinColumn(name = "id", referencedColumnName = "id"),
		    @JoinColumn(name = "cpf", referencedColumnName = "cpf")})
	private Pessoa conjuge;

	@OneToMany (mappedBy ="pessoa",fetch = FetchType.LAZY)
	private List<Email> emails;

	@OneToMany(mappedBy ="pessoa", fetch = FetchType.EAGER)
	private List<AnimalEstimacao> animaisEstimacao;
	
	@Embedded
	private Endereco endereco;
	
	@OneToOne
	private Perfil perfil;
	
	@ManyToMany
	@JoinTable(
      name="Pessoa_Projeto",
    		  joinColumns={
    		            @JoinColumn(name="id", referencedColumnName="id"),
    		            @JoinColumn(name="cpf", referencedColumnName="cpf")},
    		        inverseJoinColumns=@JoinColumn(name="idProjeto"))
	private List<Projeto> projetos;

	public PessoaId getId() {
		return id;
	}

	public void setId(PessoaId id) {
		this.id = id;
	}

	public Pessoa getConjuge() {
		return conjuge;
	}

	public void setConjuge(Pessoa conjuge) {
		this.conjuge = conjuge;
	}

	public List<Email> getEmails() {
		return emails;
	}

	public void setEmails(List<Email> emails) {
		this.emails = emails;
	}

	public List<AnimalEstimacao> getAnimaisEstimacao() {
		return animaisEstimacao;
	}

	public void setAnimaisEstimacao(List<AnimalEstimacao> animaisEstimacao) {
		this.animaisEstimacao = animaisEstimacao;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public List<Projeto> getProjetos() {
		return projetos;
	}

	public void setProjetos(List<Projeto> projetos) {
		this.projetos = projetos;
	}
	
	
}
****************************************************************************************************************

package br.gov.etec.exemplojpa.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Perfil {
	@Id
	private Integer id;

	private String descricao;

}

****************************************************************************************************************

package br.gov.etec.exemplojpa.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Funcionario {

	@Id
	private Integer id;

	private String nome;

	@ManyToOne
	@JoinColumn(name = "idDepartamento")
	private Departamento departamento;

	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "idGerente")
	private Funcionario gerente;

	@OneToMany(mappedBy = "gerente", cascade = CascadeType.ALL)
	private Set<Funcionario> subordinados = new HashSet<Funcionario>();

	@OneToMany(mappedBy = "funcionario", cascade = CascadeType.ALL)
	private Set<Atividade> atividades = new HashSet<Atividade>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

}

****************************************************************************************************************

package br.gov.etec.exemplojpa.model;

import javax.persistence.Embeddable;

@Embeddable
public class Endereco {

	private String rua;
	
	private String nome;
}


****************************************************************************************************************

package br.gov.etec.exemplojpa.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Email {
	@Id
	private Integer id;	
	
	private String endereco;
	
	@ManyToOne 
	private Pessoa pessoa;
}


****************************************************************************************************************


package br.gov.etec.exemplojpa.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Departamento {
	
	@Id
	private Integer id;
	
	private String descricao;

	@OneToMany (mappedBy = "departamento")
	private List<Funcionario> funcionarios;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}

****************************************************************************************************************

package br.gov.etec.exemplojpa.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Atividade implements Serializable {

	@Id
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idProjeto")
	private Projeto projeto;

	@Id
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idFuncionario")
	private Funcionario funcionario;

	private Date dataInicio;

}


****************************************************************************************************************

package br.gov.etec.exemplojpa.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class AnimalEstimacao {

	@Id
	private Integer id;	
	
	@ManyToOne 
	private Pessoa pessoa;
}

****************************************************************************************************************

package br.gov.etec.exemplojpa.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Acessorio {

	@Id
	private Integer id;
	
	private String descricao;
}

****************************************************************************************************************

package br.com.etec.entretec.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * The persistent class for the aluno database table.
 * 
 */
@Entity
@Table(name = "ALUNO")
public class Aluno extends Pessoa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "CEP")
	private String cep;

	@Column(name = "NUMERO")
	private String numeroCasa;

	@Column(name = "COMPLEMENTO")
	private String complemento;

	@Column(name = "PAIS")
	private String pais;

	@Column(name = "RUA")
	private String rua;

	@Column(name = "ESTADO")
	private String estado;

	@Column(name = "CIDADE")
	private String cidade;

	@Column(name = "BAIRRO")
	private String bairro;

	private String rg;
	private String cpf;

	private boolean deficiente;

	private Integer dddCelular;
	private String numeroCelular;
	private Integer dddTelefone;
	private String numeroTelefone;
	private String senha;

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public boolean isDeficiente() {
		return deficiente;
	}

	public void setDeficiente(boolean deficiente) {
		this.deficiente = deficiente;
	}

	public Integer getDddCelular() {
		return dddCelular;
	}

	public void setDddCelular(Integer dddCelular) {
		this.dddCelular = dddCelular;
	}

	public String getNumeroCelular() {
		return numeroCelular;
	}

	public void setNumeroCelular(String numeroCelular) {
		this.numeroCelular = numeroCelular;
	}

	public Integer getDddTelefone() {
		return dddTelefone;
	}

	public void setDddTelefone(Integer dddTelefone) {
		this.dddTelefone = dddTelefone;
	}

	public String getNumeroTelefone() {
		return numeroTelefone;
	}

	public void setNumeroTelefone(String numeroTelefone) {
		this.numeroTelefone = numeroTelefone;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getNumeroCasa() {
		return numeroCasa;
	}

	public void setNumeroCasa(String numeroCasa) {
		this.numeroCasa = numeroCasa;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

}

****************************************************************************************************************


