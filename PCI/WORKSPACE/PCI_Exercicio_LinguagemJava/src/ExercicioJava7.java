import java.text.DecimalFormat;

import javax.swing.JOptionPane;

public class ExercicioJava7 {
	public static void main(String[] args) {
		double n1 =0;
		double n2 =0;
		double rAdicao = 0;
		double rSubtracao = 0;
		double rMultiplicacao = 0;
		double rDivisao = 0.0;
		double rRestoDivisao = 0.0;
		
		n1 = 5;
		n2 = 2;
		
		rAdicao = n1 + n2;
		rSubtracao = n1-n2;
		rMultiplicacao = n1*n2;
		rDivisao = n1/n2;
		rRestoDivisao = n1 % n2;
		
		System.out.println("Adi��o = " +  rAdicao);
		System.out.println("Subtra��o = " +  rSubtracao);
		System.out.println("Multiplica��o = " +  rMultiplicacao);
		System.out.println("Divis�o = " +  rDivisao);
		System.out.println("Resto Divis�o = " + rRestoDivisao);
		
		
		DecimalFormat decimal = new DecimalFormat("#,###.00"); 
		System.out.println("Divis�o = " + decimal.format( rDivisao));
		
		System.out.println("Divis�o = " + String.format("%.2f", rDivisao));
		
		double resultado = n1 + n2 + n1-n2 + n1* n2 + n1 / n2;
		System.out.println("Resultado = " + resultado);
		
		if(n1 % 2 == 0)
			System.out.println("PAR");
		else
			System.out.println("IMPAR");
		
		
		double n3 = 100;
		double n4 = 25;
		
		if(n3 % 2 == 0)
			System.out.println("PAR");
		else
			System.out.println("IMPAR");
		
	}
}
