import java.util.ArrayList;
import java.util.List;

public class ExercicioJava6 {
	public static void main(String[] args) {
		List<Integer> listaNota = new ArrayList<>();
		listaNota.add(10);
		listaNota.add(5);
		listaNota.add(10);
		
		System.out.println(listaNota);
		
		for (Integer nota : listaNota) {
			if(nota>5)
				System.out.println("Aprovado");
			else
				System.out.println("Reprovado");
		}
	}
	
}
