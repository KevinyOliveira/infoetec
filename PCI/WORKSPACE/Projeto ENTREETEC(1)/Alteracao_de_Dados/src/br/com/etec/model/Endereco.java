package br.com.etec.model;

public class Endereco {
	private String rua;
	private String bairro;
	private Integer cEP;
	private Integer numero;
	
	public String getRua() {
		return rua;
	}
	public void setRua(String rua) {
		this.rua = rua;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public Integer getcEP() {
		return cEP;
	}
	public void setcEP(Integer cEP) {
		this.cEP = cEP;
	}
	public Integer getNumero() {
		return numero;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
}
