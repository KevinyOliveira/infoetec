create database ENTRETEC;
use ENTRETEC;
	
	create table endereco (
		cep numeric (8,0) identity primary key, 
		rua varchar (50),
		bairro varchar (15),
		numero numeric (5,0),)

	create table usuario (
		id int identity primary key,
		cep numeric (8,0), 
		celular varchar (12),
		telefone varchar (11),
		email varchar (25),
		foreign key (cep)
		references endereco,)
	
	create table tipousuario (
		idUsuario identity primary key,
		nome varchar (50),)

	create table aluno (
		id int identity primary key references usuario (id)
		)

	create table professor (
		id int identity primary key references usuario (id)
		)
