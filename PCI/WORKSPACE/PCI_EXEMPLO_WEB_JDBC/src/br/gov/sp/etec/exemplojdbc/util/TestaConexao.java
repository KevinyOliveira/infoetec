package br.gov.sp.etec.exemplojdbc.util;

import java.sql.Connection;
import java.sql.SQLException;

import br.gov.sp.etec.exemplojdbc.dao.AlunoDAO;
import br.gov.sp.etec.exemplojdbc.model.Aluno;

public class TestaConexao {

	public static void main(String[] args) throws SQLException {
		Connection connection = new ConnectionFactory().getConnection();
		System.out.println("conexao aberta");
		connection.close();
		Aluno aluno = new Aluno();
		aluno.setNome("teste");
		aluno.setEmail("horacio.ligabue@etec.sp.gov");
		AlunoDAO dao = new AlunoDAO();
		dao.insere(aluno);
	
	}

}
