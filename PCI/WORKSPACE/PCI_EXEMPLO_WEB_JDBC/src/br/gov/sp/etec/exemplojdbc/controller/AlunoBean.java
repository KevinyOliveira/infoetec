package br.gov.sp.etec.exemplojdbc.controller;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.gov.sp.etec.exemplojdbc.dao.AlunoDAO;
import br.gov.sp.etec.exemplojdbc.model.Aluno;

@ManagedBean
@ViewScoped
public class AlunoBean {

	private Aluno aluno = new Aluno();
	private AlunoDAO dao = new AlunoDAO();
	private List<Aluno> alunos;
	
	public Aluno getAluno() {
		return aluno;
	}
	
	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public String save(){
		if (aluno.getId() == 0)
			dao.insere(aluno);
		else
			dao.altera(aluno);
		this.aluno= new Aluno();
		this.alunos = dao.getLista();
		return "alunos?faces-redirect=true";
	}
	
	public void remove(){
		dao.remove(aluno);
		this.aluno= new Aluno();
		this.alunos = dao.getLista();
	}
	
	public List<Aluno> getAlunos(){
		if (alunos==null){
			alunos = dao.getLista();
		}
		return alunos;
	}
}
