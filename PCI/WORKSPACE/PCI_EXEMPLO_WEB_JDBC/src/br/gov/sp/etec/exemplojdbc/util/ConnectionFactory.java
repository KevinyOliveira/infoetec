package br.gov.sp.etec.exemplojdbc.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

	public Connection getConnection() throws SQLException {
		
		try {
			return DriverManager.getConnection("jdbc:mysql://localhost/pc", "root", "123456");
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		
	}

}
