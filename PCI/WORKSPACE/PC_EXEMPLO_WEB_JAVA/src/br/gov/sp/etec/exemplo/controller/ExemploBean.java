package br.gov.sp.etec.exemplo.controller;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import br.gov.sp.etec.exemplo.model.Exemplo;

@ManagedBean
public class ExemploBean {
	
	private Exemplo exemplo = new Exemplo();
	
	
	public Exemplo getExemplo() {
		return exemplo;
	}


	public void setExemplo(Exemplo exemplo) {
		this.exemplo = exemplo;
	}


	public void exibirMsg() {
		  FacesContext context = FacesContext.getCurrentInstance();
	         
		 context.addMessage(null, new FacesMessage("Successo!!!!!\n"  +  exemplo.getDescricao()) );
	 }
}
