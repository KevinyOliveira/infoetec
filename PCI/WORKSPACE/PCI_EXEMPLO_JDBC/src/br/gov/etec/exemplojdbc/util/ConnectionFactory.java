package br.gov.etec.exemplojdbc.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

		public Connection getConnection() throws SQLException {
			
			try {
				String url = "jdbc:sqlserver://localhost\\SQLEXPRESS:1433;databaseName=PC";
				return DriverManager.getConnection(url, "sa", "123456");
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
			
			
		}

}
