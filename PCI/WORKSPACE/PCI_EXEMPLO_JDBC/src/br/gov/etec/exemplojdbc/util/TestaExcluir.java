package br.gov.etec.exemplojdbc.util;

import java.sql.Connection;
import java.sql.SQLException;

import br.gov.etec.exemplojdbc.dao.AlunoDAO;
import br.gov.etec.exemplojdbc.model.Aluno;

public class TestaExcluir {
	public static void main(String[] args) throws SQLException {
		Connection connection = new ConnectionFactory().getConnection();
		System.out.println("conexao aberta");
		connection.close();
		Aluno aluno = new Aluno();
		aluno.setId(1);
		AlunoDAO dao = new AlunoDAO();
		dao.remove(aluno);
		System.out.println("Dados removidos com sucesso!");
	}

}
