package br.gov.etec.exemplojdbc.util;

import java.sql.Connection;
import java.sql.SQLException;

import br.gov.etec.exemplojdbc.dao.AlunoDAO;
import br.gov.etec.exemplojdbc.model.Aluno;

public class TestaAtualizar {

	public static void main(String[] args) throws SQLException {
		Connection connection = new ConnectionFactory().getConnection();
		System.out.println("conexao aberta");
		connection.close();
		Aluno aluno = new Aluno();
		aluno.setNome("Horacio");
		aluno.setEmail("horacio.ligabue@etec.sp.gov");
		AlunoDAO dao = new AlunoDAO();
		dao.altera(aluno);
		System.out.println("Dados atualizados com sucesso!");
	}

}
