package br.gov.etec.exemplojdbc.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.gov.etec.exemplojdbc.dao.AlunoDAO;
import br.gov.etec.exemplojdbc.model.Aluno;

public class TestaListar {
	public static void main(String[] args) throws SQLException {
		Connection connection = new ConnectionFactory().getConnection();
		System.out.println("conexao aberta");
		connection.close();
		List<Aluno> lista = new ArrayList<Aluno>();	
		AlunoDAO dao = new AlunoDAO();
		lista = dao.getLista();
		System.out.println("Dados listados com sucesso!");
		System.out.println(lista.get(0).getNome());
		
		for (Aluno aluno : lista) {
			System.out.println(aluno.getNome());
		}
	}

}
