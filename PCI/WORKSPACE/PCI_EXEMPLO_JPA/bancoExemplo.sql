create database PC;
use PC;

	create table Trabalho (
		id_Trabalho integer not null primary key auto_increment,
        descricao varchar(50));
                
	create table Responsavel_Trabalho (
        fk_Trabalho integer not null,
		fk_Responsavel integer not null, 
		foreign key(fk_Trabalho) references Trabalho(id_Trabalho),
		foreign key (fk_Responsavel)	references Responsavel(id));
