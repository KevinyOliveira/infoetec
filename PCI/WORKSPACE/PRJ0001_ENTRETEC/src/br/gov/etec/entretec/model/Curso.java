package br.gov.etec.entretec.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="CURSO")
public class Curso {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private String descricao;
	private List<Disciplina> disciplinas;
	private Boolean ativo;
	private PlanoCurso planoCurso;
	private Coordenador coordenador;
	private RegimentoCurso regimentoCurso;
	
}
