package br.gov.etec.entretec.infra;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {
	private static EntityManagerFactory entityManagerFactory = 
			Persistence.createEntityManagerFactory("ENTRETEC");

	public EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}
}
