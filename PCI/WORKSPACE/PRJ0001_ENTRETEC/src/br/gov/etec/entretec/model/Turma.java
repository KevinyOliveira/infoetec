package br.gov.etec.entretec.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.gov.etec.entretec.util.PeriodoEnum;

@Entity
@Table(name="TURMA")
public class Turma {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private Curso curso; 
	private String descricao;
	private PeriodoEnum periodo;
	private Date dataInicio;
	private Date dataFinal;
	
	}
