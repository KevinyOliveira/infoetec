package br.gov.etec.entretec.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.gov.etec.entretec.util.GeneroMusica;
import br.gov.etec.entretec.util.Hobbies;
@Entity
@Table(name="PERIFL")
public class Perfil {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private Usuario usuario;
	private List<GeneroMusica> generoMusicas;
	private List<Esporte> esportes;
	private List<Hobbies> hobbies;
	private List<CursoExtraCurricular> cursosExtraCurricular;
	private List<Expectativa> expectativas;
	private List<Conhecimento> conhecimentos;
	private List<ExperienciaProfissional> experienciasProfissional;
	
}
