package br.gov.etec.entretec.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="AUTORIZACAO_EVENTO")
public class AutorizacaoEvento {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private Coordenador coordenador;
	private Boolean autorizacao;
	private String parecerAutorizacao;
}
