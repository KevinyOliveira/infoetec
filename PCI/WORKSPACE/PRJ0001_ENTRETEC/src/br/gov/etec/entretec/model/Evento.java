package br.gov.etec.entretec.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="EVENTO")
public class Evento {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private String nome;
	private String descricao;
	private Date dataEvento;
	private ResponsavelEvento responsavelEvento;
	private AutorizacaoEvento autorizacaoEvento;
	private LocalEvento local;
	
}
