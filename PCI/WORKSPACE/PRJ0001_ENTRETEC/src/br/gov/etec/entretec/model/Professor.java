package br.gov.etec.entretec.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.primefaces.model.UploadedFile;
@Entity
@Table(name="PROFESSOR")
public class Professor {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private String nome;
	private String sexo;
	private String email;
	private String emailCorporativo;
	private String celular;
	private UploadedFile foto;
	private Date dataNascimento;
	private Curriculo curriculo;
	private Perfil perfil;
	private List<Disciplina> disciplinas;
	
}
