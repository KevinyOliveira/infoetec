package br.gov.etec.entretec.bean;
import javax.faces.bean.ManagedBean;

import br.gov.etec.entretec.model.Usuario;

@ManagedBean
class ProfessorBean{
	
private Usuario Usuario = new Usuario();
private String texto;

public String getTexto() {
	return texto;
}

public void setTexto(String texto) {
	this.texto = texto;
}

public Usuario getUsuario() {
	return Usuario;
}

public void setUsuario(Usuario professor) {
	this.Usuario = professor;
}

public void exibirInfo() {
	this.texto = Usuario.getLogin();
}


}