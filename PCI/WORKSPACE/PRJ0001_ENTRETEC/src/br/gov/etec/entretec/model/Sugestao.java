package br.gov.etec.entretec.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity



public class Sugestao {

	
	@Id
	@GeneratedValue
	private int id;
	private String tipoSugestao;
	private Boolean anonimo;
	private String descricao;
	
	public String getTipoSugestao() {
		return tipoSugestao;
	}
	public void setTipoSugestao(String tipoSugestao) {
		this.tipoSugestao = tipoSugestao;
	}
	public Boolean getAnonimo() {
		return anonimo;
	}
	public void setAnonimo(Boolean anonimo) {
		this.anonimo = anonimo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}

