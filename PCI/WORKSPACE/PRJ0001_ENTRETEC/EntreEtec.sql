create database Entretec;
use Entretec;

create table Aluno (
	idAluno int auto_increment,
	bairro varchar (50),
    email varchar (50),
    celular varchar (15),
    rua varchar (50),
    telefone varchar (50),
    cpf varchar (50),
    numeroCasa numeric (4,0)
    primary key (idAluno));