package br.gov.etec.exemplojpa.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.gov.etec.exemplojpa.model.Aluno;

public class AlunoDAO {

	private final DAO<Aluno> dao;
	
	public AlunoDAO(EntityManager em) {
		dao = new DAO<Aluno>(em,Aluno.class);
	}

	public void adiciona(Aluno t) {
		dao.adiciona(t);
	}

	public Aluno buscaPorId(Integer id) {
		return dao.buscaPorId(id);
	}

	public List<Aluno> lista() {
		return dao.lista();
	}

	public void remove(Aluno t) {
		dao.remove(t);
	}

	public void altera(Aluno t) {
		dao.altera(t);
	}		
}
