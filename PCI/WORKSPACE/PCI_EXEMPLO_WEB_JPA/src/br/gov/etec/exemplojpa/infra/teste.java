package br.gov.etec.exemplojpa.infra;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.gov.etec.exemplojpa.model.Aluno;

public class teste {
	public static void main(String[] args) {
		Aluno aluno = new Aluno();
        aluno.setNome("X");

        EntityManagerFactory factory = Persistence.
                    createEntityManagerFactory("exemplo");
        EntityManager manager = factory.createEntityManager();

        manager.getTransaction().begin();        
        manager.persist(aluno);
        manager.getTransaction().commit();    

        System.out.println("ID da tarefa: " + aluno.getId());

        manager.close();
    }
        
}
