package br.com.etec.business;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.etec.dao.UsuarioDAO;
import br.com.etec.infra.JPAUtil;
import br.com.etec.model.Usuario;

public class UsuarioRN {
Usuario usuario = new Usuario();
	List<Usuario> usuarios = new ArrayList<Usuario>();
	
	/*
	public void gravar(Usuario entity) {
		EntityManager em = new JPAUtil().getEntityManager();
        em.getTransaction().begin();
        UsuarioDAO dao = new UsuarioDAO(em);
        dao.adiciona(entity);
        em.getTransaction().commit();
        em.close();
	}
	 */
	
	public void atualizar(Usuario usuario) {
		EntityManager em = new JPAUtil().getEntityManager();
        em.getTransaction().begin();
        UsuarioDAO dao = new UsuarioDAO(em);
		dao.altera(usuario);
		em.getTransaction().commit();
	    em.close();
	    
	}
	 /*
	
	public void excluir(Usuario usuario) {
		EntityManager em = new JPAUtil().getEntityManager();
        em.getTransaction().begin();
        UsuarioDAO dao = new UsuarioDAO(em);
        Usuario usuarioExcluido = dao.busca(usuario.getId());
        dao.remove(usuarioExcluido);
        em.getTransaction().commit();
        em.close();
        usuario = new Usuario();
     }
	
	public List<Usuario> listar() {
		EntityManager em = new JPAUtil().getEntityManager();
        em.getTransaction().begin();
        UsuarioDAO dao = new UsuarioDAO(em);
        usuario = dao.lista();
        em.close();
        
		return usuario;
	}

*/
	public Usuario getEntity() {
		return usuario;
	}

	public void setEntity(Usuario entity) {
		this.usuario = entity;
	}

	public List<Usuario> getEntities() {
		return (List<Usuario>) usuarios;
	}

	public void setEntities(List<Usuario> entities) {
		this.usuario = (Usuario) entities;
	}

}
