package br.com.etec.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Professor extends Usuario{
	
	public Professor(String celular, String telefone, String email) {
		super(celular, telefone, email);
	}
	
	@Id
	private Long idProfessor;

	public Long getIdProfessor() {
		return idProfessor;
	}

	public void setIdProfessor(Long idProfessor) {
		this.idProfessor = idProfessor;
	}
	
	@OneToOne
	@JoinTable(name="usuario",joinColumns={@JoinColumn(name="idUsuario")})
	private Usuario usuario;
	
	@OnetoMany
	private List<MiniCurriculo> miniCurriculo;
}
