package br.com.etec.model;

//import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
//import javax.persistence.Inheritance;
import javax.persistence.OneToMany;

@Entity
/*@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
	public abstract class TipoUsuario implements Serializable {*/

public class TipoUsuario {
	
	@Id
	@GeneratedValue
    private Long idUsuario;
    private String nome;
	public Long getIdPessoa() {
		return idUsuario;
	}
	public void setIdPessoa(Long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
    
    @OneToMany (mappedBy="tipoUsuario")
    private Usuario usuario;
}