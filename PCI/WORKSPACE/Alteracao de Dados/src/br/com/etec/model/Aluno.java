package br.com.etec.model;

public class Aluno extends Usuario {
	
	public Aluno(String celular, String telefone, String email) {
		super(celular, telefone, email);
	}
      
	@Id
	private Long idAluno;

	public Long getIdAluno() {
		return idAluno;
	}

	public void setIdAluno(Long idAluno) {
		this.idAluno = idAluno;
	}
	
	@OneToOne
	@JoinTable(name="usuario",joinColumns={@JoinColumn(name="idUsuario")})
	private Usuario usuario;
}