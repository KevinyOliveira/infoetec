package br.com.etec.controller;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.ToggleEvent;
import org.primefaces.model.UploadedFile;

import br.com.etec.model.Endereco;
import br.com.etec.model.Usuario;

@ManagedBean
public class AlterarDadosBean {
     
	private Endereco endereco = new Endereco();
	private Usuario login = new Usuario();
	private UploadedFile abrirpasta;
	private UploadedFile escolherarquivo;

		public Endereco getEndereco() {
		return endereco;
		}

		public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
		}

		public UploadedFile getEscolherarquivo() {
		return escolherarquivo;
		}

		public void setEscolherarquivo(UploadedFile escolherarquivo) {
		this.escolherarquivo = escolherarquivo;
		}

		public UploadedFile getAbrirpasta() {
		return abrirpasta;
		}

		public void setAbrirpasta(UploadedFile abrirpasta) {
		this.abrirpasta = abrirpasta;
		}

		public Usuario getLogin() {
		return login;
		}

		public void setLogin(Usuario login) {
		this.login = login;
		}
		
    public void upload1(ToggleEvent event) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Toggled", "Visibility:" + event.getVisibility());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
	
    public void upload2(ToggleEvent event) {
    	FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Toggled", "Visibility:" + event.getVisibility());
    FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void buttonAction(ActionEvent actionEvent) {
    	FacesContext context = FacesContext.getCurrentInstance();
        
        context.addMessage(null, new FacesMessage("Successful",  "Your message: ") );
        context.addMessage(null, new FacesMessage("Second Message", "Additional Message Detail"));
    }
}