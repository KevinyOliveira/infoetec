package br.com.etec.dao;

import java.util.List;

public class AlunoDao {
	    private MinhaClasse objeto;
	    private Boolean editando = false;

	   //construtores e forma��o da tela, posicionamento e tudo mais omitido.


	   //verificar edicao.
	   public void preencheCampos(MinhaClasse objeto){
	        this.objeto = objeto;

	       //Coloca aqui todos os preenchimento dos campos.
	       txtNome.setText(objeto.getNome()); //exemplo

	       editando = true;
	   }

	   public void clickDoBotaoSalvar(){
	        if (this.objeto == null){ //Caso n�o seja edi��o precisa criar um novo objeto.
	           this.objeto = new MinhaClasse();
	        }

	        //agora preencha os dados no objeto.
	        this.objeto.setNome(txtNome.getText());         

	        if (editando) {
	           dao.update(this.objeto);
	        } else {
	           dao.insert(this.objeto);
	        }
	   }
}