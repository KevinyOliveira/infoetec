package br.com.etec.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.etec.model.Usuario;

public class UsuarioDAO {

	private final DAO<Usuario> dao;

	public UsuarioDAO(EntityManager em) {
		dao = new DAO<Usuario>(em, Usuario.class);
	}
/*
	public void adiciona(Usuario t) {
		dao.adiciona(t);
	}

	public Usuario busca(Integer id) {
		return dao.busca(id);
	}
*/
	public List<Usuario> lista() {
		return dao.lista();
	}
/*
	public void remove(Usuario t) {
		dao.remove(t);
	}
*/
	public void altera(Usuario conta) {
		dao.altera(conta);
	}
}
