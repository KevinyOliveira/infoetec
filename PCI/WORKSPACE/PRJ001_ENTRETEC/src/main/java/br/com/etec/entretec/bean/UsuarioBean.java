package br.gov.etec.entretec.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.event.ActionEvent;

import br.gov.etec.entretec.bussiness.UsuarioRN;
import br.gov.etec.entretec.model.Usuario;

@ManagedBean

public class UsuarioBean {
	private Usuario usuario = new Usuario();

	UsuarioRN serviceUsuario = new UsuarioRN();
	
	public void gravar(ActionEvent actionEvent) {
		if(usuario.getId() == null){
			serviceUsuario.gravar(usuario);
        }
	}
       

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
