package br.com.entretec.business;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.entretec.DAO.AvaliacaoDAO;
import br.com.entretec.infra.JPAUtil;
import br.com.entretec.model.Avaliacao;

public class AvaliacaoRN {

	Avaliacao Avaliacao = new Avaliacao();
	List<Avaliacao> solicitacoes = new ArrayList<Avaliacao>();
	
	public void gravar(Avaliacao entity) {
		EntityManager em = new JPAUtil().getEntityManager();
        em.getTransaction().begin();
        AvaliacaoDAO dao = new AvaliacaoDAO(em);
        dao.adiciona(entity);
        em.getTransaction().commit();
        em.close();
	}

	
}
