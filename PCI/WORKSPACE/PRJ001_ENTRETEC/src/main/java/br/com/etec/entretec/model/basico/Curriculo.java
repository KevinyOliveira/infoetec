package br.com.etec.entretec.model.basico;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="CURRICULO")
public class Curriculo {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="NM_ALUNO")
	private Integer id;
	
	@Column(name="NM_ALUNO")
	private String docCurriculo;
	
	@OneToMany (mappedBy = "curriculo")
	private List<Aluno> alunos;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDocCurriculo() {
		return docCurriculo;
	}

	public void setDocCurriculo(String docCurriculo) {
		this.docCurriculo = docCurriculo;
	}

	public List<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}
}
