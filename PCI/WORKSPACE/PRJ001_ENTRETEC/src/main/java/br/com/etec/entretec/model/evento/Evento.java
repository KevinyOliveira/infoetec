package br.com.etec.evento.mode;

import javax.persistence.Entity;

import org.primefaces.component.log.Log;

@Entity
public class Evento {
		public Log id;
		public String apelido;
		public Double foto;
		public Double sigla;
		public String integrantes;
		
		
		public Log getId() {
			return id;
		}
		public void setId(Log id) {
			this.id = id;
		}
		public String getApelido() {
			return apelido;
		}
		public void setApelido(String apelido) {
			this.apelido = apelido;
		}
		public double getFoto() {
			return foto;
		}
		public void setFoto(double foto) {
			this.foto = foto;
		}
		public double getSigla() {
			return sigla;
		}
		public void setSigla(double sigla) {
			this.sigla = sigla;
		}
		public String getIntegrantes() {
			return integrantes;
		}
		public void setIntegrantes(String integrantes) {
			this.integrantes = integrantes;
		}
		
		
}