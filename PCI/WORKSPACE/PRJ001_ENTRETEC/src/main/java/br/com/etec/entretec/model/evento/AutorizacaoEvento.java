package br.com.etec.entretec.model.evento;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.etec.entretec.model.basico.Coordenador;

@Entity
@Table(name="AUTORIZACAO_EVENTO")
public class AutorizacaoEvento {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	private Coordenador coordenador;
	
	
	private Boolean autorizacao;
	
	
	private String parecerAutorizacao;
}
