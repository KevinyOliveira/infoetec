package br.com.etec.entretec.model.basico;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name="PERFIL")
public class Perfil {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private Integer id;
	
	@OneToMany (mappedBy = "perfil")
	private List<Aluno> alunos;
	
	@ManyToOne
	private GeneroMusica generoMusica;
	
	@ManyToOne
	private Esporte esporte;
	
	@ManyToOne
	private Hobbies hobbie;
	
	@ManyToMany
	private List<FormacaoAcademica> formacaoAcademica;
 	
	@Column
	private List<CursoExtraCurricular> cursosExtraCurricular;
	
	@Column
	private List<Expectativa> expectativas;
	
	@Column
	private List<Conhecimento> conhecimentos;
	
	@Column
	private List<ExperienciaProfissional> experienciasProfissional;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}

	public List<CursoExtraCurricular> getCursosExtraCurricular() {
		return cursosExtraCurricular;
	}

	public void setCursosExtraCurricular(List<CursoExtraCurricular> cursosExtraCurricular) {
		this.cursosExtraCurricular = cursosExtraCurricular;
	}

	public List<Expectativa> getExpectativas() {
		return expectativas;
	}

	public void setExpectativas(List<Expectativa> expectativas) {
		this.expectativas = expectativas;
	}


	public List<Conhecimento> getConhecimentos() {
		return conhecimentos;
	}

	public void setConhecimentos(List<Conhecimento> conhecimentos) {
		this.conhecimentos = conhecimentos;
	}

	public List<ExperienciaProfissional> getExperienciasProfissional() {
		return experienciasProfissional;
	}

	public void setExperienciasProfissional(List<ExperienciaProfissional> experienciasProfissional) {
		this.experienciasProfissional = experienciasProfissional;
	}
	
	
	
}
