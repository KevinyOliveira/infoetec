package br.gov.etec.entretec.bean;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.ToggleEvent;
import org.primefaces.model.UploadedFile;

@ManagedBean
public class AlterarDadosBean {
     
	//private Login login = new Login();
	private UploadedFile abrirpasta;
	private UploadedFile escolherarquivo;
	
	
		public UploadedFile getEscolherarquivo() {
		return escolherarquivo;
		}

		public void setEscolherarquivo(UploadedFile escolherarquivo) {
		this.escolherarquivo = escolherarquivo;
		}

		public UploadedFile getAbrirpasta() {
		return abrirpasta;
		}

		public void setAbrirpasta(UploadedFile abrirpasta) {
		this.abrirpasta = abrirpasta;
		}

			
    public void upload1(ToggleEvent event) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Toggled", "Visibility:" + event.getVisibility());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
	
    public void upload2(ToggleEvent event) {
    	FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Toggled", "Visibility:" + event.getVisibility());
    FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void buttonAction(ActionEvent actionEvent) {
    
    }
    
}