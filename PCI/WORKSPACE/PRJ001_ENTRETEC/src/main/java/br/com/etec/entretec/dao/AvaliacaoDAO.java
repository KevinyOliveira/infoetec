package br.com.entretec.DAO;

import javax.persistence.EntityManager;

import br.com.entretec.model.Avaliacao;

public class AvaliacaoDAO {

	private final DAO<Avaliacao> dao;

	public AvaliacaoDAO(EntityManager em) {
		dao = new DAO<Avaliacao>(em, Avaliacao.class);
	}

	public void adiciona(Avaliacao t) {
		dao.adiciona(t);
	}
	
}
