package br.com.entretec.business;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.entretec.DAO.ReclamacaoDAO;
import br.com.entretec.infra.JPAUtil;
import br.com.entretec.model.*;

public class ReclamacaoRN {

	Reclamacao reclamacao = new Reclamacao();
	List<Reclamacao> reclamacoes = new ArrayList<Reclamacao>();
	
	
	public void gravar(Reclamacao entity) {
		EntityManager em = new JPAUtil().getEntityManager();
        em.getTransaction().begin();
        ReclamacaoDAO dao = new ReclamacaoDAO(em);
        dao.adiciona(entity);
        em.getTransaction().commit();
        em.close();
	}

	
}
