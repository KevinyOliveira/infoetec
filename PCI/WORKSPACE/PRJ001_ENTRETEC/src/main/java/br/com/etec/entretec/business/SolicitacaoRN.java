package br.com.entretec.business;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.entretec.DAO.SolicitacaoDAO;
import br.com.entretec.infra.JPAUtil;
import br.com.entretec.model.Solicitacao;

public class SolicitacaoRN {

	Solicitacao solicitacao = new Solicitacao();
	List<Solicitacao> solicitacoes = new ArrayList<Solicitacao>();
	
	public void gravar(Solicitacao entity) {
		EntityManager em = new JPAUtil().getEntityManager();
        em.getTransaction().begin();
        SolicitacaoDAO dao = new SolicitacaoDAO(em);
        dao.adiciona(entity);
        em.getTransaction().commit();
        em.close();
	}

	
}
