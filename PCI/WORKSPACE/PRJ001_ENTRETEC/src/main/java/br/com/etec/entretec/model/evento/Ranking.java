package br.com.etec.evento.mode;

import javax.persistence.Entity;

@Entity
public class Ranking {
		private Long id;
		private Double colocacao;
		private String nomeDoTime;
		private String participantes;
		private int vitorias;
		private int derrotas;
		private Double gols;
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public Double getColocacao() {
			return colocacao;
		}
		public void setColocacao(Double colocacao) {
			this.colocacao = colocacao;
		}
		public String getNomeDoTime() {
			return nomeDoTime;
		}
		public void setNomeDoTime(String nomeDoTime) {
			this.nomeDoTime = nomeDoTime;
		}
		public String getParticipantes() {
			return participantes;
		}
		public void setParticipantes(String participantes) {
			this.participantes = participantes;
		}
		public int getVitorias() {
			return vitorias;
		}
		public void setVitorias(int vitorias) {
			this.vitorias = vitorias;
		}
		public int getDerrotas() {
			return derrotas;
		}
		public void setDerrotas(int derrotas) {
			this.derrotas = derrotas;
		}
		public Double getGols() {
			return gols;
		}
		public void setGols(Double gols) {
			this.gols = gols;
		}
}
