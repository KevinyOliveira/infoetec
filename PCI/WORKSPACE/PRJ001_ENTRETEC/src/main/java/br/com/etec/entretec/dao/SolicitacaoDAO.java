package br.com.entretec.DAO;

import javax.persistence.EntityManager;
import br.com.entretec.model.*;

public class SolicitacaoDAO {

	private final DAO<Solicitacao> dao;

	public SolicitacaoDAO(EntityManager em) {
		dao = new DAO<Solicitacao>(em, Solicitacao.class);
	}

	public void adiciona(Solicitacao t) {
		dao.adiciona(t);
	}
	
}
