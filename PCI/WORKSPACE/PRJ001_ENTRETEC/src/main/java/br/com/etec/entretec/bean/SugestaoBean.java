package br.gov.etec.entretec.bean;

import br.gov.etec.entretec.model.Sugestao;

public class SugestaoBean {
	private Sugestao sugestao = new Sugestao();

	public Sugestao getSugestao() {
		return sugestao;
	}

	public void setSugestao(Sugestao sugestao) {
		this.sugestao = sugestao;
	}
}
