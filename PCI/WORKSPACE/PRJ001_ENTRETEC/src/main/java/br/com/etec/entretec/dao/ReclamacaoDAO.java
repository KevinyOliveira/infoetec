package br.com.entretec.DAO;

import javax.persistence.EntityManager;

import br.com.entretec.model.Reclamacao;

public class ReclamacaoDAO {

	private final DAO<Reclamacao> dao;

	public ReclamacaoDAO(EntityManager em) {
		dao = new DAO<Reclamacao>(em, Reclamacao.class);
	}

	public void adiciona(Reclamacao t) {
		dao.adiciona(t);
	}
	
}
