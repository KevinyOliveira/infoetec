package br.com.etec.entretec.model.basico;

import java.util.List;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Cascade;

import br.com.etec.entretec.model.basico.Cachorro.Pessoa;
import br.com.etec.entretec.model.basico.Cachorro.Pessoa.Pessoa.Endereco.Pessoa.Trabalho;
import br.com.etec.entretec.model.basico.Cachorro.Pessoa.Pessoa.Endereco.Pessoa.Trabalho.Pessoa.Projeto.Project;
import br.com.etec.entretec.model.basico.Cachorro.Pessoa.Pessoa.Endereco.Pessoa.Trabalho.Pessoa.Projeto.Usuario.DependentId;
import br.com.etec.entretec.model.basico.Cachorro.Pessoa.Pessoa.Endereco.Pessoa.Trabalho.Pessoa.Projeto.Usuario.Employee;
import br.com.etec.entretec.model.basico.Cachorro.Pessoa.Pessoa.Endereco.Pessoa.Trabalho.Pessoa.Projeto.Usuario.Group;
import br.com.etec.entretec.model.basico.Cachorro.Pessoa.Pessoa.Endereco.Pessoa.Trabalho.Pessoa.Projeto.Usuario.GroupMemberShip;
import br.com.etec.entretec.model.basico.Cachorro.Pessoa.Pessoa.Endereco.Pessoa.Trabalho.Pessoa.Projeto.Usuario.GroupMemberShipId;
import br.com.etec.entretec.model.basico.Cachorro.Pessoa.Pessoa.Endereco.Pessoa.Trabalho.Pessoa.Projeto.Usuario.User;

