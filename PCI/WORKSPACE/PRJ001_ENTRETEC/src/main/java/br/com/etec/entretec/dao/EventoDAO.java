package br.com.etec.entretec.dao;

import javax.persistence.EntityManager;

private final DAO<evento> dao;

public AlunoDAO(EntityManager em) {
	dao = new DAO<evento>(em, evento.class);
}

public void adiciona(evento t) {
	dao.adiciona(t);
}

public void remove(evento t) {
	dao.remove(t);
}

public void altera(evento conta) {
	dao.altera(conta);
}

public Aluno busca(Integer id) {
	return dao.busca(id);
}
	
}
