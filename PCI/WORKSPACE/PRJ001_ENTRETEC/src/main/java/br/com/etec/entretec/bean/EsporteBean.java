package br.gov.etec.entretec.bean;

import javax.faces.bean.ManagedBean;

import br.gov.etec.entretec.model.PesquisaEsporte;

@ManagedBean
public class EsporteBean {
	
	private PesquisaEsporte pe = new PesquisaEsporte();

	public PesquisaEsporte getPe() {
		return pe;
	}

	public void setPe(PesquisaEsporte pe) {
		this.pe = pe;
	}
	

}
