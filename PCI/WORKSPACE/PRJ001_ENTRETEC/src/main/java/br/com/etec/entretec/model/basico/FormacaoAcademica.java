package br.com.etec.entretec.model.basico;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.etec.entretec.util.Status;

@Entity
public class FormacaoAcademica {
	@Id
	private Integer id;
	
	private String descricao;
	
	private Date anoConclusao;
	
	private Status status;

}
