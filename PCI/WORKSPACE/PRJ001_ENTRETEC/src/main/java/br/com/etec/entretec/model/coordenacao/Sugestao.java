package br.com.etec.entretec.model.coordenacao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SUGESTAO")
public class Sugestao {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private Integer id;
	
	@Column(name="DESCRICAO")
	private String descricao;
	
	@Column(name="ID_TIPO_SUGESTAO")
	private TipoSugestao tipoSugestao;
	
	@Column(name="ANONIMO")
	private Boolean anonimo;
	
	@Column(name="ID_TIPO_CATEGORIA_SUGESTAO")	
	private CategoriaSugestao categoriaSugestao;
	
	public TipoSugestao getTipoSugestao() {
		return tipoSugestao;
	}
	public void setTipoSugestao(TipoSugestao tipoSugestao) {
		this.tipoSugestao = tipoSugestao;
	}
	public Boolean getAnonimo() {
		return anonimo;
	}
	public void setAnonimo(Boolean anonimo) {
		this.anonimo = anonimo;
	}
	
	
		public Integer getId() {
			return id;
		}
	
		public void setId(Integer id) {
			this.id = id;
		}
	
		public String getDescricao() {
			return descricao;
		}
	
		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
}

