package br.com.etec.entretec.model.basico;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="EXPERIENCIA_PROFISSIONAL")
public class ExperienciaProfissional {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private String descricao;
	
	private Integer tempoMeses;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getTempoMeses() {
		return tempoMeses;
	}

	public void setTempoMeses(Integer tempoMeses) {
		this.tempoMeses = tempoMeses;
	}
	
	
}
