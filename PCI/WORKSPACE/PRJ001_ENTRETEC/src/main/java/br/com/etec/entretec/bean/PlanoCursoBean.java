package br.gov.etec.entretec.bean;
 
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
 
@ManagedBean
public class PlanoCursoBean {
         
    private String[] selectedCities;
    private String[] selectedCities2;
    private List<String> cursos;
    private String cursoSelecionado;
   
     
    @PostConstruct
    public void init() {
        cursos = new ArrayList<String>();
        cursos.add("Eletr�nica");
        cursos.add("'Administra��o");
        cursos.add("Loig�stica");
    

    }
 
    public String[] getSelectedCities() {
        return selectedCities;
    }
 
    public void setSelectedCities(String[] selectedCities) {
        this.selectedCities = selectedCities;
    }
    public String[] getSelectedCities2() {
        return selectedCities2;
    }
 
    public void setSelectedCities2(String[] selectedCities2) {
        this.selectedCities2 = selectedCities2;
    }

	public String getCursoSelecionado() {
		return cursoSelecionado;
	}

	public void setCursoSelecionado(String cursoSelecionado) {
		this.cursoSelecionado = cursoSelecionado;
	}

	public List<String> getCursos() {
		return cursos;
	}

	public void setCursos(List<String> cursos) {
		this.cursos = cursos;
	}
 
	
	
   
}