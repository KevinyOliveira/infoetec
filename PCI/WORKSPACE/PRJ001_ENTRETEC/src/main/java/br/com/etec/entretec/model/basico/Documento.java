package br.com.etec.entretec.model.basico;

import javax.persistence.Column;

public class Documento {
	
	@Column(name="ID")
	private Integer id;
	
	@Column(name="DESCRICAO")
	private String descricao;
	
	@Column(name="DOC_LOCAL")
	private String docLocal;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDocLocal() {
		return docLocal;
	}

	public void setDocLocal(String docLocal) {
		this.docLocal = docLocal;
	}
	
	
	

}
