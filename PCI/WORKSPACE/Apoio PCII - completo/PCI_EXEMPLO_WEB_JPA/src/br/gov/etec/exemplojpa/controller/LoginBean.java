package br.gov.etec.exemplojpa.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;

import br.gov.etec.exemplojpa.dao.UsuarioDAO;
import br.gov.etec.exemplojpa.infra.JPAUtil;
import br.gov.etec.exemplojpa.model.Usuario;

@ManagedBean
@SessionScoped
public class LoginBean {
	
	private Usuario usuario = new Usuario();

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public String efetuarLogin() {
		boolean loginValido = true;
		if (loginValido){
			return "alunos?faces-redirect=true";
		}
		else{
			this.usuario = new Usuario();
			
		}
		
		return "login";
	}
	
}
