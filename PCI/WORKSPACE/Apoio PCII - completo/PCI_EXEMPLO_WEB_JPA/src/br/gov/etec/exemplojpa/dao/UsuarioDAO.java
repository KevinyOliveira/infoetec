package br.gov.etec.exemplojpa.dao;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.gov.etec.exemplojpa.model.Usuario;

public class UsuarioDAO {

	private final EntityManager em;

	public UsuarioDAO(EntityManager em) {
		this.em = em;
	}

	public boolean existe(Usuario usuario) {

		Query query = em
				.createQuery("select u from Usuario u where u.login = :pLogin and u.senha = :pSenha");
		query.setParameter("pLogin", usuario.getLogin());
		query.setParameter("pSenha", usuario.getSenha());

		boolean encontrado = !query.getResultList().isEmpty();

		return encontrado;
	}
}