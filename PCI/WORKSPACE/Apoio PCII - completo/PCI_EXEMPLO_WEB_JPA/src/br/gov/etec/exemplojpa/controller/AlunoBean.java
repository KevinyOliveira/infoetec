package br.gov.etec.exemplojpa.controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;

import br.gov.etec.exemplojpa.dao.AlunoDAO;
import br.gov.etec.exemplojpa.infra.JPAUtil;
import br.gov.etec.exemplojpa.model.Aluno;

@ManagedBean
@ViewScoped
public class AlunoBean {

	private Aluno aluno = new Aluno();
	private List<Aluno> alunos = new ArrayList<Aluno>();

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public String grava() {
		EntityManager em = new JPAUtil().getEntityManager();
		em.getTransaction().begin();
		AlunoDAO dao = new AlunoDAO(em);

		if (aluno.getId() == 0)
			dao.adiciona(aluno);
		else
			dao.altera(aluno);
		this.aluno = new Aluno();
		this.alunos = dao.lista();

		em.getTransaction().commit();
		em.close();
		return "alunos?faces-redirect=true";
	}

	
	public void remove() {
		EntityManager em = new JPAUtil().getEntityManager();
		em.getTransaction().begin();
		AlunoDAO dao = new AlunoDAO(em);
		
		Aluno alunoParaRemover = dao.buscaPorId(this.aluno.getId());
		dao.remove(alunoParaRemover);
		this.aluno = new Aluno();
		this.alunos = dao.lista();
		em.flush();
		em.getTransaction().commit();
		em.close();
	}

	public List<Aluno> getAlunos() {
		EntityManager em = new JPAUtil().getEntityManager();
		AlunoDAO dao = new AlunoDAO(em);

		if (alunos == null) {
			alunos = dao.lista();
		}

		em.close();
		return alunos;
	}
}
