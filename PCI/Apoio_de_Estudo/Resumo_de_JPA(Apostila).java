RESUMO JPA HIBERNATE
Revisão de Banco de Dados:

Um aluno pode ter diversas matérias 
Cada matéria pode ter diversos alunos
Cada professor pode ter várias turmas
Uma turma pode ter vários professores
Um curso pode ter muitas turmas
Cada turma deve ter um curso

ORM – JPA – Hibernate
	
@OneToOne
@ManyToOne
@OneToMany (mappedBy=”__”)
@ManyToMany
  @JoinTable(
      name="__ ",
      joinColumns=@JoinColumn(name="__ ", referencedColumnName="__"),
      inverseJoinColumns=@JoinColumn(name="__ ", referencedColumnName="__"))
@Basic (fetch = FetchType.LAZY)

jpql
native query
criteria query
cascade
select p from Pessoa p
EAGER
 @Basic (fetch = FetchType.LAZY)

Cascade		detached		insert, update, delete				
Qualquer persist, merge e remove que for acionada em uma entity deve ser refletida para as demais Entities relacionadas
CascadeType.ALL					
* cuidado						
remover todos os relacionamentos, lentidão		
sempre executar na entity que foi configurado		




@Entity
public class Pessoa {
	
	@EmbeddedId
    private PessoaId id;
		
	@OneToOne
	@JoinColumns(value = {
		    @JoinColumn(name = "id", referencedColumnName = "id"),
		    @JoinColumn(name = "cpf", referencedColumnName = "cpf")})
	private Pessoa conjuge;

	@OneToMany (mappedBy ="pessoa",fetch = FetchType.LAZY)
	private List<Email> emails;

	@OneToMany(mappedBy ="pessoa", fetch = FetchType.EAGER)
	private List<AnimalEstimacao> animaisEstimacao;
	//Criou a classe endereco 
	@Embedded
	private Endereco endereco;
	//Relacionamento de 'um pra um' Nota: Só um lado é necessário a anotação...
	@OneToOne
	private Perfil perfil;
	//Entendendo o funcionameto da tabela asssociativa no Java...
	@ManyToMany
	//criacao de tabela associativa
	@JoinTable(
      name="Pessoa_Projeto",
		//tabela recebendo chaves dos dois "lados"...
    		  joinColumns={
    		            @JoinColumn(name="id", referencedColumnName="id"),
    		            @JoinColumn(name="cpf", referencedColumnName="cpf")},
    		        inverseJoinColumns=@JoinColumn(name="idProjeto"))
	private List<Projeto> projetos;
}
//"Mapeação dupla"(unica) de chaves primarias compostas* 
@Embeddable
public class PessoaId implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Integer id;
		
	private Integer cpf;
}


@Embeddable
public class Endereco {

	private String rua;
	
	private String nome;
}


@Entity
public class Email {
	@Id
	private Integer id;	
	
	private String endereco;
	
	@ManyToOne 
	private Pessoa pessoa;
}

@Entity
public class Perfil {
	@Id
	private Integer id;
	
	private String descricao;
}

@Entity
public class Usuario {
	@Id
	private Integer id;
	
	private String login;
	
	private String senha;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="idFuncionario")	
	private Funcionario funcionario;
}

@Entity
public class Projeto {
	@Id
	private Integer id;
	
	private String descricao;
}

@Entity
public class Departamento {
	
	@Id
	private Integer id;
	
	private String descricao;

	@OneToMany (mappedBy = "departamento")
	private List<Funcionario> funcionarios;
}

@Entity
public class Funcionario {

	@Id
	private Integer id;
	
	private String nome;
	
	@ManyToOne
	@JoinColumn(name ="idDepartamento")
	private Departamento departamento;
	
	@ManyToOne(cascade={ CascadeType.ALL})
    @JoinColumn(name="idGerente")
    private Funcionario gerente;

	@OneToMany(mappedBy="gerente", cascade = CascadeType.ALL)
    private Set<Funcionario> subordinados = new HashSet<Funcionario>();
	
	@OneToMany(mappedBy="funcionario", cascade=CascadeType.ALL)
	private Set<Atividade> atividades = new HashSet<Atividade>();
}

@Entity
public class Atividade implements Serializable {
	
	@Id
    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="idProjeto")
    private Projeto projeto;
    
	@Id
    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="idFuncionario")
    private Funcionario funcionario; 
    
    private Date dataInicio;
}

@Entity
public class Veiculo {
	@Id
	private Integer id;
	
	@ManyToMany
	@JoinTable(name = "Veiculo_Acessorios",
	joinColumns = @JoinColumn(name = "idVeiculo"),
	inverseJoinColumns = @JoinColumn(name = "idAcessorio"))
	private Set<Acessorio> acessorios;
}

@Entity
public class Acessorio {

	@Id
	private Integer id;
	
	private String descricao;
}


public class Teste {
	public static void main(String[] args) {
		//teste para inserir
		
			
		EntityManager em = new JPAUtil().getEntityManager();
        em.getTransaction().begin();
      
        Departamento departamento = new Departamento();
        departamento.setId(10);
        departamento.setDescricao("Secretária");
        
        em.persist(departamento);
        
        Funcionario funcionario = new Funcionario();
        funcionario.setId(1);
        funcionario.setNome("Joao");
        funcionario.setDepartamento(departamento);
        em.persist(funcionario);
        
        em.getTransaction().commit();
        em.close();
		
	}
}


