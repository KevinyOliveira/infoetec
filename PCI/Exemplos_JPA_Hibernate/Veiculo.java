package br.gov.etec.exemplojpa.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Veiculo {
	@Id
	private Integer id;
	
	@ManyToMany
	@JoinTable(name = "Veiculo_Acessorios",
	joinColumns = @JoinColumn(name = "idVeiculo"),
	inverseJoinColumns = @JoinColumn(name = "idAcessorio"))
	private Set<Acessorio> acessorios;
}
