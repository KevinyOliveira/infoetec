package br.gov.etec.exemplojpa.model;

import javax.persistence.EntityManager;

public class Teste {
	public static void main(String[] args) {
		//teste para inserir
		
			
		EntityManager em = new JPAUtil().getEntityManager();
        em.getTransaction().begin();
      
        Departamento departamento = new Departamento();
        departamento.setId(10);
        departamento.setDescricao("Secretária");
        
        em.persist(departamento);
        
        Funcionario funcionario = new Funcionario();
        funcionario.setId(1);
        funcionario.setNome("Joao");
        funcionario.setDepartamento(departamento);
        em.persist(funcionario);
        
        em.getTransaction().commit();
        em.close();
		
	}

}
