package br.gov.etec.exemplojpa.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Funcionario {

	@Id
	private Integer id;
	
	private String nome;
	
	@ManyToOne
	@JoinColumn(name ="idDepartamento")
	private Departamento departamento;
	
	@ManyToOne(cascade={ CascadeType.ALL})
    @JoinColumn(name="idGerente")
    private Funcionario gerente;

	@OneToMany(mappedBy="gerente", cascade = CascadeType.ALL)
    private Set<Funcionario> subordinados = new HashSet<Funcionario>();
	
	 @OneToMany(mappedBy="funcionario", cascade=CascadeType.ALL)
	 private Set<Atividade> atividades = new HashSet<Atividade>();
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	
	
	
	
}
