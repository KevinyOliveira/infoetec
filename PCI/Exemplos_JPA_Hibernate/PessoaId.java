package br.gov.etec.exemplojpa.model;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class PessoaId implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private Integer id;
	
	
	private Integer cpf;

}
