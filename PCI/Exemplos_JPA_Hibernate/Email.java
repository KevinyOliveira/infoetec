package br.gov.etec.exemplojpa.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Email {
	@Id
	private Integer id;	
	
	private String endereco;
	
	@ManyToOne 
	private Pessoa pessoa;
}
