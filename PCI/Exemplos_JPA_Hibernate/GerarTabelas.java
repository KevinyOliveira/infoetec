package br.gov.etec.exemplojpa.model;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class GerarTabelas {

    public static void main(String[] args) {
        EntityManagerFactory factory = Persistence.
                    createEntityManagerFactory("EXEMPLO");

        factory.close();
    }
}
