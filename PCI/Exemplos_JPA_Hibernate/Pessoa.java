package br.gov.etec.exemplojpa.model;

import java.util.List;

import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Pessoa {
	@EmbeddedId
    private PessoaId id;
		
	@OneToOne
	@JoinColumns(value = {
		    @JoinColumn(name = "id", referencedColumnName = "id"),
		    @JoinColumn(name = "cpf", referencedColumnName = "cpf")})
	private Pessoa conjuge;

	@OneToMany (mappedBy ="pessoa",fetch = FetchType.LAZY)
	private List<Email> emails;

	@OneToMany(mappedBy ="pessoa", fetch = FetchType.EAGER)
	private List<AnimalEstimacao> animaisEstimacao;
	
	@Embedded
	private Endereco endereco;
	
	@OneToOne
	private Perfil perfil;
	
	@ManyToMany
	@JoinTable(
      name="Pessoa_Projeto",
    		  joinColumns={
    		            @JoinColumn(name="id", referencedColumnName="id"),
    		            @JoinColumn(name="cpf", referencedColumnName="cpf")},
    		        inverseJoinColumns=@JoinColumn(name="idProjeto"))
	private List<Projeto> projetos;

	public PessoaId getId() {
		return id;
	}

	public void setId(PessoaId id) {
		this.id = id;
	}

	public Pessoa getConjuge() {
		return conjuge;
	}

	public void setConjuge(Pessoa conjuge) {
		this.conjuge = conjuge;
	}

	public List<Email> getEmails() {
		return emails;
	}

	public void setEmails(List<Email> emails) {
		this.emails = emails;
	}

	public List<AnimalEstimacao> getAnimaisEstimacao() {
		return animaisEstimacao;
	}

	public void setAnimaisEstimacao(List<AnimalEstimacao> animaisEstimacao) {
		this.animaisEstimacao = animaisEstimacao;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public List<Projeto> getProjetos() {
		return projetos;
	}

	public void setProjetos(List<Projeto> projetos) {
		this.projetos = projetos;
	}
	
	
}
