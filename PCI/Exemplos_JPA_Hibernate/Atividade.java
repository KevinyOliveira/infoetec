package br.gov.etec.exemplojpa.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Atividade implements Serializable {
	
	@Id
    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="idProjeto")
    private Projeto projeto;
    
	@Id
    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="idFuncionario")
    private Funcionario funcionario; 
    
    private Date dataInicio;
    

}
