package br.gov.etec.exemplojpa.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Usuario {
	@Id
	private Integer id;
	
	private String login;
	
	private String senha;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="idFuncionario")	
	private Funcionario funcionario;

}
