﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXE_IntroducaoOO
{
    class Program
    {
        static void Main(string[] args)
        {
            Aluno aluno = new Aluno();
            aluno.id = 10;
            aluno.nome = "Joao";
            aluno.notas.Add(10);
            aluno.notas.Add(5);
            aluno.notas.Add(3);

            int media = (aluno.notas[0] + aluno.notas[1] + aluno.notas[2]) / 2;

            Console.WriteLine("Nota do aluno: " + aluno.nome + " Média: " + media);
            Console.ReadLine();


        }
    }
}
