﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetoConexaoDados.Models;

namespace ProjetoConexaoDados
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Display();
        }

        public void Display()
        {
            DSIEntities1 _entity = new DSIEntities1();
            
            List<ProdutoInformation> _productList = 
                new List<ProdutoInformation>();

            _productList = _entity.PRODUTO.Select(x => 
                                new ProdutoInformation
            {
                Cod_Prod = x.COD_PROD,
                Nome = x.NOME,
                Peso = x.PESO,
                Preco_Custo = x.PRECO_CUSTO,
                Preco_Venda = x.PRECO_VENDA,
                //Tipo_Produto=x.TIPO_PRODUTO
            }).ToList();

            dataGridView1.DataSource = _productList;
            
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            this.ClearFields();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            PRODUTO prd = new PRODUTO();

            prd.NOME        = txtNome.Text;
            prd.PESO        = Convert.ToDecimal(txtPeso.Text);
            prd.PRECO_CUSTO = Convert.ToDecimal(txtPrecoCusto.Text);
            prd.PRECO_VENDA = Convert.ToDecimal(txtPrecoVenda.Text);
            //prd.TIPO_PRODUTO = cmbTipoProduto.SelectedItem.ToString();

            bool result = SaveProduct(prd);  
            ShowStatus(result, "Save");
        }

        public bool SaveProduct(PRODUTO prd)  
        {
            bool result = false;
            DSIEntities1 _entity = new DSIEntities1();
            
            _entity.PRODUTO.Add(prd);
            _entity.SaveChanges();
            result = true;
            
            return result;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in dataGridView1.SelectedRows)  
                {
                    txtCodigo.Text      = row.Cells[0].Value.ToString();
                    txtNome.Text        = row.Cells[1].Value.ToString();
                    txtPeso.Text        = row.Cells[2].Value.ToString();
                    txtPrecoCusto.Text  = row.Cells[3].Value.ToString();
                    txtPrecoVenda.Text  = row.Cells[4].Value.ToString();
                }
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            PRODUTO prd = SetValues(Convert.ToInt32(txtCodigo.Text),
                                    txtNome.Text,
                                    Convert.ToDecimal(txtPeso.Text),
                                    Convert.ToDecimal(txtPrecoCusto.Text),
                                    Convert.ToDecimal(txtPrecoVenda.Text));

            bool result = UpdateProduct(prd);  
            ShowStatus(result, "Update");
        }

        public bool UpdateProduct(PRODUTO prd)  
        {
            bool result = false;
            using (DSIEntities1 _entity = new DSIEntities1())
            {
                PRODUTO prdUpdate = _entity.PRODUTO.Where(x => 
                x.COD_PROD == prd.COD_PROD).Select(x => x).FirstOrDefault();

                prdUpdate.NOME = prd.NOME;
                prdUpdate.PESO = prd.PESO;
                prdUpdate.PRECO_CUSTO = prd.PRECO_CUSTO;
                prdUpdate.PRECO_VENDA = prd.PRECO_VENDA;
                //prdUpdate.TIPO_PRODUTO = prd.TIPO_PRODUTO;
                _entity.SaveChanges();
                result = true;
            }
            return result;
        }

        private void btnDeletar_Click(object sender, EventArgs e)
        {
            PRODUTO prd = SetValues(Convert.ToInt32(txtCodigo.Text),
                                    txtNome.Text,
                                    Convert.ToDecimal(txtPeso.Text),
                                    Convert.ToDecimal(txtPrecoCusto.Text),
                                    Convert.ToDecimal(txtPrecoVenda.Text));
                                    //cmbTipoProduto.SelectedItem.ToString());  
            bool result = DeleteProduct(prd); 
            ShowStatus(result, "Delete");
        }

        public bool DeleteProduct(PRODUTO prd)  
        {
            bool result = false;
            using (DSIEntities1 _entity = new DSIEntities1())
            {
                PRODUTO _product = _entity.PRODUTO.Where(x => 
                x.COD_PROD == prd.COD_PROD).Select(x => x).FirstOrDefault();
                _entity.PRODUTO.Remove(_product);
                _entity.SaveChanges();
                result = true;
            }
            return result;
        }

        public PRODUTO SetValues(   int     Cod_Prod, 
                                    string  Nome, 
                                    decimal Peso, 
                                    decimal Preco_Custo, 
                                    decimal Preco_Venda)
                                     
        {
            PRODUTO prd = new PRODUTO();

            prd.COD_PROD = Cod_Prod;
            prd.NOME = Nome;
            prd.PESO = Peso;
            prd.PRECO_CUSTO = Preco_Custo;
            prd.PRECO_VENDA = Preco_Venda;
    
            return prd;
        }

        public void ShowStatus(bool result, string Action)  
        {
            if (result)
            {
                if (Action.ToUpper() == "SAVE")
                {
                    MessageBox.Show("Salvo com Sucesso!..", "Salvar", 
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (Action.ToUpper() == "UPDATE")
                {
                    MessageBox.Show("Alterado com Sucesso!..", "Alterar", 
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Excluído com Sucesso!..", "Deletar", 
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Algo aconteceu de errado!. Tente novamente!..", 
                    "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            ClearFields();
            Display();
        }

        public void ClearFields()  
        {
            txtCodigo.Text = "";
            txtNome.Text = "";
            txtPeso.Text = "";
            txtPrecoCusto.Text = "";
            txtPrecoVenda.Text = "";
            //cmbTipoProduto.SelectedItem = "";
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
