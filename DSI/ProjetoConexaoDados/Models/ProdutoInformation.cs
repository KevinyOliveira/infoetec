﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoConexaoDados.Models
{
    public class ProdutoInformation
    {
        public int Cod_Prod { get; set; }
        public string Nome { get; set; }
        public decimal Peso { get; set; }
        public decimal Preco_Venda { get; set; }
        public decimal Preco_Custo { get; set; }
        //public string  Tipo_Produto { get; set; }
    }
}
