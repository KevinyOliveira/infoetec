﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevisaoLogica
{
    class Program
    {
        static void Main(string[] args)
        {
            double n1, n2, n3, m;
            Console.WriteLine("Digite a primeira nota:");
            n1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Digite a segunda nota:");
            n2 = int.Parse(Console.ReadLine());
            Console.WriteLine("Digite a terceira nota:");
            n3 = int.Parse(Console.ReadLine());

            m = (n1 + n2 + n3) / 3;

            if (m >= 7)
                Console.WriteLine("Aprovado");
            else
                Console.WriteLine("Reprovado");

            Console.ReadLine();
        }
    }
}
