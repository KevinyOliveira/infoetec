﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication02.Models
{
    public class Aluno
    {
        public String NomeAluno { get; set  }
        public String Endereco { get; set }
        public String Email { get; set } 
    }
}