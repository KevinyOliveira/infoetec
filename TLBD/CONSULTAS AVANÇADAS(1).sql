﻿-- 1 - Revisão de SQL – DML e DDL

SELECT * FROM CUSTOMERS

UPDATE CUSTOMERS 
SET Region = 'SP', Phone = '(11)9999-9999',Fax = '(11)9999-9998'
WHERE CustomerID = 'ROBSO'

DELETE Customers
WHERE CustomerID = 'ROBS1'

--2. Sub-queries, inner join, outer join e self join
--SUB-QUERIES
SELECT (SELECT COUNT(od.OrderID) FROM [Order Details] od
WHERE o.OrderID = od.OrderID) AS 'Number Lines'
,* 
FROM ORDERS o

SELECT * FROM [Order Details]
WHERE OrderID = 10248

--INNER JOIN

-- OUTER JOIN

-- SELF JOIN


--DQL – Linguagem de Consulta de Dados:
--o where, Visto no exemplo anterior

--have,


SELECT Employees.LastName, COUNT(Orders.OrderID) AS NumberOfOrders FROM (Orders
INNER JOIN Employees
ON Orders.EmployeeID=Employees.EmployeeID)
GROUP BY LastName
HAVING COUNT(Orders.OrderID) > 10;
--between,

SELECT * FROM Products
WHERE UnitPrice BETWEEN 10 AND 20; 
--order by,

 
--in, 


métodos
específicos (getdate,
entre outros),
operadores lógicos,
relacionais e aritméticos

tipos de dados;
 funções:
o convert(), str(float,
lenght, decimal),
ascii(string),
char(integer), len(string),
lower(string),
upper(string),
replicate(string, integer),
space
(nº_espaço_em_branco)
, right(string, nº de
caracteres à esquerda),
left (string, nº de
caracteres à direita),
ltrim(string), rtrim
(string), substring(string
texto, posicao_inicial,
tamanho),
reverse(string), dateadd
(parte, numero, data),
round(número, precisão,
arredondar ou truncar),
isnull(valor1, valor a ser
retornado),
isnumeric(expressao),
case, count, avg(
[distinct │all] n), max(
[distinct │all] n), min(
[distinct │all] n), sum(
[distinct │all] n)

junções:
o associações internas –
inner join (junções
idênticas e nãoidênticas);
o associações externas –
left outer join ou right
outer join, full outer join;
o associações cruzadas –
cross join;
o auto-junção