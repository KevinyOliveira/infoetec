SELECT * FROM Orders Ord
WHERE Ord.OrderID = 10248

SELECT * FROM [Order Details] OrdDet
WHERE OrdDet.OrderID = 10248

SELECT OrdDet.OrderID,OrdDet.ProductID, Ord.OrderID, Ord.OrderDate 
FROM [ORDER DETAILS] OrdDet
INNER JOIN Orders Ord
ON OrdDet.OrderID = Ord.OrderID
WHERE OrdDet.OrderID = 10248

SELECT * FROM Orders Ord
WHERE Ord.OrderID = 11078


SELECT OrdDet.OrderID,OrdDet.ProductID, Ord.OrderID, Ord.OrderDate 
FROM [ORDER DETAILS] OrdDet
LEFT OUTER JOIN Orders Ord
ON OrdDet.OrderID = Ord.OrderID
WHERE OrdDet.OrderID = 11078

SELECT OrdDet.OrderID,OrdDet.ProductID, Ord.OrderID, Ord.OrderDate 
FROM [ORDER DETAILS] OrdDet
RIGHT OUTER JOIN Orders Ord
ON OrdDet.OrderID = Ord.OrderID
WHERE OrdDet.OrderID = 11078

SELECT OrdDet.OrderID,OrdDet.ProductID, Ord.OrderID, Ord.OrderDate 
FROM [ORDER DETAILS] OrdDet
FULL OUTER JOIN Orders Ord
ON OrdDet.OrderID = Ord.OrderID
WHERE OrdDet.OrderID = 11078



SELECT OrdDet.OrderID,OrdDet.ProductID, Ord.OrderID, Ord.OrderDate 
FROM Orders Ord
RIGHT OUTER JOIN  [ORDER DETAILS] OrdDet
ON Ord.OrderID = OrdDet.OrderID 
WHERE Ord.OrderID = 11078

SELECT OrdDet.OrderID,OrdDet.ProductID, Ord.OrderID, Ord.OrderDate 
FROM Orders Ord
LEFT OUTER JOIN  [ORDER DETAILS] OrdDet
ON Ord.OrderID = OrdDet.OrderID 
WHERE Ord.OrderID = 11078


SELECT OrdDet.OrderID,OrdDet.ProductID, Ord.OrderID, Ord.OrderDate 
FROM [ORDER DETAILS] OrdDet
FULL OUTER JOIN Orders Ord
ON OrdDet.OrderID = Ord.OrderID
WHERE OrdDet.OrderID = 10248

UPDATE [Order Details]
SET OrderID = 11079
WHERE OrderID = 10324


SELECT Ord.OrderID, Ord.OrderDate,Ord.CustomerID, Cust.CustomerID
FROM Orders Ord
INNER JOIN  Customers Cust
ON Ord.CustomerId = Cust.CustomerID 

SELECT Ord.OrderID, Ord.OrderDate,Ord.CustomerID, Cust.CustomerID
FROM Orders Ord
LEFT OUTER JOIN  Customers Cust
ON Ord.CustomerId = Cust.CustomerID 

SELECT Ord.OrderID, Ord.OrderDate,Ord.CustomerID, Cust.CustomerID
FROM Orders Ord
FULL OUTER JOIN  Customers Cust
ON Ord.CustomerId = Cust.CustomerID 


