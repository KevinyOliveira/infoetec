create database DBCinema;

use DBCinema;


create table sala (
numero_sal int,
capacidade_sal int not null,
primary key (numero_sal));

create table genero (
codigo_gen int identity,
nome_gen varchar(50) not null,
primary key (codigo_gen));

create table ator(
codigo_ator int identity,
nome_ator varchar(50) not null,
primary key (codigo_ator));

create table filme (
codigo_fil int identity,
titulo_fil varchar(60),
duracao_fil datetime,
primary key (codigo_fil));

create table filme_ator(
codigo_fil int,
codigo_ator int,
primary key(codigo_fil, codigo_ator),
foreign key (codigo_fil) references filme,
foreign key (codigo_ator) references ator);

create table sessao(
codigo_ses int identity,
vl_inteira_ses numeric(10,2),
vl_meia_ses numeric(10,2),
codigo_fil int,
primary key (codigo_ses),
foreign key(codigo_fil) references filme);

create table sala_sessao(
codigo_ses int,
codigo_sal int,
tot_ingressos int,
primary key (codigo_ses, codigo_sal),
foreign key (codigo_ses) references sessao,
foreign key (codigo_sal) references sala);


